How to launch: 

    1. ( OPTIONAL ) Download docker with mysql: 
        $ docker pull mysql

    2. ( OPTIONAL ) Start container:
        $ docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=testdb -e      MYSQL_USER=admin -e MYSQL_PASSWORD=root -d mysql:latest

    3. Create schemas and tables in mysql

        CREATE SCHEMA `db_A` ;
        CREATE TABLE `db_A`.`Person` (
            `id` INT NOT NULL AUTO_INCREMENT,
              `FirstName` VARCHAR(45) NOT NULL,
              `LastName` VARCHAR(45) NOT NULL,
              `Age` VARCHAR(45) NOT NULL,
              PRIMARY KEY (`id`));

        CREATE SCHEMA `db_B` ;
        CREATE TABLE `db_B`.`logs` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `changed_id` INT NOT NULL,
        `edited_column` VARCHAR(45) NOT NULL,
        `old_value` VARCHAR(45) NOT NULL,
        `new_value` VARCHAR(45) NOT NULL,
        `time_of_change` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`));

    
    4. Compile and run server and client in separate terminals:
        $ ./server
        $ ./client 
    
    5. Now you are able to use command showed by client : 
        example usage :
        STAT
        INC 10 (number)
        GET 50 (age)
        SLEEP 1 (time in s)
        END
        WRITE Jan Kowalski 10 / WRITE 110 (id of existing record)  Firstname (column to change) Janusz (new name) 
                // this operation also save logs of updated records in other db
        READ 110 (existing object_id)
        DEL 110(existing object_id)
  
    
