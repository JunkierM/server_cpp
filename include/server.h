#pragma onc

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>

#include <iostream>
#include "db_operations.h"
#include <chrono>
#include "json/json.h"
#include <thread>
#include "helpers.h"


#define PORT 8080

struct ClientData
{
    int socket;
    std::chrono::steady_clock::time_point start_time;
};

class Server : public Helpers
{

public:
    bool startServer();
    ServerQuery readClientMsg();
    Json::Value createJsonObject(std::string data);
    void sendResponse(Json::Value data);
    Json::Value creatStatJson(std::string data);
    Json::Value createObject(Person person);

private:

    long _getUptime();
    long _getClientUptime(int client);
    int _master_socket, _addrlen, _new_socket, _client_socket[30],
        _max_clients = 30, _activity, _i, _valread, _sd, _max_sd, _opt = 1;
    fd_set _readfds;
    struct sockaddr_in _address;
    std::vector<ClientData> _client_times;
    ServerQuery _parseJsonToQuery(Json::Value client_msg);

    char _buffer[1024];
    std::chrono::steady_clock::time_point _start_time;
    std::chrono::steady_clock::time_point _last_connection_time;
};
