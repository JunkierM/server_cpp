#include "helpers.h"

void Helpers::printDataQuery(ServerQuery query)
{

    std::cout << "\nServerQuery query: " << query.query << "\nServerQuery query_type: " << query.query_type << "\nServerQuery type: " << query.type << "\n\n";
}

void Helpers::printPerson(Person person)
{

    std::cout << " id : " << person.id << "\n name: " << person.name << "\n lastname: " << person.lastname << "\n age: " << person.age << "\n";
}

Json::Value Helpers::_decodeMessage(const char *buffer)
{

    std::string strJson(buffer);

    Json::CharReaderBuilder builder;
    Json::CharReader *reader = builder.newCharReader();

    Json::Value response;
    std::string errors;

    bool parsingSuccessful = reader->parse(
        strJson.c_str(),
        strJson.c_str() + strJson.size(),
        &response,
        &errors);
    delete reader;

    if (!parsingSuccessful)
    {
        // std::cout << "Message was corrupted." << std::endl;
        response["status"] = "error";
        // std::cout << errors << std::endl;
        return response;
    }

    return response;
}