cmake_minimum_required(VERSION 3.16)
project(zadanie)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_BUILD_TYPE RELEASE)

find_package(Boost REQUIRED)
find_package(jsoncpp REQUIRED)


include_directories( 
    include
    ${Boost_INCLUDE_DIR} 
    /usr/include/
)

add_library(${PROJECT_NAME}
  src/db_operations.cpp
  src/helpers.cpp
)

target_link_libraries(${PROJECT_NAME}
  mysqlcppconn
  jsoncpp_lib
)


add_executable(server src/server.cpp)
target_link_libraries(server
  ${PROJECT_NAME}
  jsoncpp_lib
)

add_executable(client src/client.cpp)
target_link_libraries(client
  jsoncpp_lib  
  ${PROJECT_NAME}
)




