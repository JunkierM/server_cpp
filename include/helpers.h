#pragma once
#include <string.h>
#include <iostream>
#include <vector>
#include "json/json.h"

enum Command
{
    STAT,
    INC,
    GET,
    SLEEP,
    END,
    WRITE,
    READ,
    DEL,
    UPDATE,
    NONE
};
struct Person
{
    int id;
    std::string name;
    std::string lastname;
    int age;
};
struct ServerQuery
{
    std::string query;
    std::string query_type;
    Command type;
    Person person;
};

class Helpers
{
public:
    void printDataQuery(ServerQuery query);
    void printPerson(Person person);
    const std::string TABLE_NAME{"db_A.Person"};

protected:
    // void _decodeMessage(const char *buffer);
    Json::Value _decodeMessage(const char *buffer);
    std::vector<std::string> _commands = {"STAT", "INC", "GET", "SLEEP", "END", "WRITE", "READ", "DEL"};
};