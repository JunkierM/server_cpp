#ifndef DB_OPERATIONS_H
#define DB_OPERATIONS_H

//mysql
#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/exception.h>
#include "helpers.h"
#include <vector>
#include <ostream>

#define IPADRESS "127.0.0.1:3306"
#define USERNAME "root"
#define PASSWORD "root"

class DataBase : public Helpers
{
public:
    void createConnectionToDB(std::string db_name);
    void getAllDataFromDB(std::string table);

    Person readDataFromDB(ServerQuery in_query);
    std::string getDataFromDB(ServerQuery query);
    std::string deleteDataFromDB(ServerQuery query);
    std::string writeDataToDB(ServerQuery in_query);
    std::string updateDataToDB(ServerQuery in_query);
    void saveLogs(ServerQuery in_query, Person person);


private:
    sql::Driver *driver;
    sql::Connection *connection;
    sql::Statement *statement;
    sql::ResultSet *result;
    sql::PreparedStatement *preparedStatement;
    int _getObjCount(ServerQuery in_query);
};

#endif