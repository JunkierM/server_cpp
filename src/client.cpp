#include "client.h"

std::vector<std::string> Client::getCommands()
{
    return _commands;
}

bool Client::connectToServer()
{
    if ((_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return false;
    }

    _serv_addr.sin_family = AF_INET;
    _serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, SERVER_ADRESS, &_serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address\n");
        return false;
    }

    if (connect(_sock, (struct sockaddr *)&_serv_addr, sizeof(_serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return false;
    }
    _start_time = std::chrono::steady_clock::now();
    return true;
}

void Client::_sendData(Json::Value data)
{

    std::string temp_data = data.toStyledString();
    const char *c = temp_data.c_str();
    send(_sock, c, strlen(c), 0);
}

void Client::_readData()
{
    _valread = read(_sock, _buffer, 1024);
    Json::Value response;
    response = _decodeMessage(_buffer);
    if (!response.isNull())
    {
        std::cout << std::endl
                  << response.toStyledString()
                  << std::endl;
    }
}

bool Client::selectCommand(std::string input)
{

    Command command;
    std::vector<std::string> inputs;
    Json::Value req;
    inputs = _parseCommand(input);
    for (size_t i = 0; i < _commands.size(); i++)
        if (inputs[0] == _commands[i])
        {
            command = static_cast<Command>(i);
            req["cmd"] = _commands[i];
            break;
        }
    switch (command)
    {
    case STAT:
        sendReadOperation(1, inputs, req, command);
        break;
    case INC:
        sendReadOperation(2, inputs, req, command);
        break;
    case GET:

        sendReadOperation(2, inputs, req, command);
        break;

    case SLEEP:
        sendReadOperation(2, inputs, req, command);
        break;
    case END:
        sendReadOperation(1, inputs, req, command);
        return true;
        break;
    case WRITE:
        sendReadOperation(inputs.size(), inputs, req, command);
        break;
    case READ:
        sendReadOperation(2, inputs, req, command);
        break;
    case DEL:
        sendReadOperation(2, inputs, req, command);
        break;

    default:
        std::cout << "Wrong formula. Please try again.\n";
        printIntro();
        break;
    }
    return false;
}

void Client::sendReadOperation(int arg, std::vector<std::string> inputs, Json::Value &req, Command command)
{
    bool stop_action = false;

    if (inputs.size() != arg)
    {
        printIntro();
    }
    else
    {
        if (command == STAT)
        {

            long time = _getUptime();
            req["args"] = createSimpleRequest(time);
        }
        else if (command == INC || command == GET)
        {

            long get_input = std::stol(inputs[1]);
            req["args"] = createSimpleRequest(get_input);
        }
        else if (command == SLEEP)
        {

            int get_input = std::stoi(inputs[1]);
            req["args"] = createSimpleRequest(get_input);
        }
        else if (command == END)
        {
        }
        else if (command == WRITE)
        {
            if (inputs.size() == 2)
            {
                Person person;
                person.id = std::stoi(inputs[1]);
                _getValuesForUpdate(person);
                req["args"] = createObject(person);
            }
            else if (inputs.size() == 4)
            {
                if (_hasOnlyDigits(inputs[3]))
                    req["args"] = createObject(inputs[1], inputs[2], inputs[3]);
                else
                    stop_action = true;
            }
        }
        else if (command == READ || command == DEL)
            req["args"] = createSimpleRequest(inputs[1]);
        if (!stop_action)
        {
            _sendData(req);
            _readData();
        }
        else
        {
            std::cout << "\nSomething goes wrong. Please try again.\n\n";
            printIntro();
        }
    }
}
bool Client::_hasOnlyDigits(const std::string s)
{
    return s.find_first_not_of("0123456789") == std::string::npos;
}
void Client::_getValuesForUpdate(Person &person)
{
    bool get_args = false;
    std::vector<std::string> commands;

    while (!get_args)
    {
        std::cout << "Please type value to change :<column_name new_value>\n";
        std::string data;
        std::getline(std::cin, data);
        commands = _parseCommand(data);
        if (commands.size() == 2)
        {
            get_args = true;
            if (commands[0] == "FirstName")
                person.name = commands[1];
            else if (commands[0] == "LastName")
                person.lastname = commands[1];
            else if (commands[0] == "Age")
                person.age = std::stoi(commands[1]);
            else
            {
                get_args = false;
            }
        }
    }
}

std::vector<std::string> Client::_parseCommand(std::string input)
{
    std::vector<std::string> inputs;

    std::stringstream ss;
    std::string segment;
    ss << input;

    while (std::getline(ss, segment, ' '))
        inputs.push_back(segment);
    return inputs;
}

void Client::printIntro()
{

    std::cout << "\nPlease type one from below option to get result \n";
    _all_commands = " STAT,\n INC <number>,\n GET <number> -returns same age in db, \n SLEEP <sleep time> -sleep for while,\n END - disconnect,\n WRITE <key> <new value> - to add new object type(name,lastname,age)\n                     - to edit type id then column and new walue,\n READ <key>,\n DEL <key>\n\n";
    std::cout << _all_commands;
}
long Client::_getUptime()
{

    auto current_time = std::chrono::steady_clock::now();
    auto uptime = std::chrono::duration_cast<std::chrono::seconds>(current_time - _start_time);
    long x = uptime.count();
    return x;
}

Json::Value Client::createObject(std::string first_name, std::string last_name, std::string age)
{

    Json::Value j_person;
    j_person["FirstName"] = first_name;
    j_person["LastName"] = last_name;
    j_person["Age"] = age;

    return j_person;
}
Json::Value Client::createObject(Person person)
{

    Json::Value j_person;
    j_person["id"] = person.id;
    j_person["FirstName"] = person.name;
    j_person["LastName"] = person.lastname;
    j_person["Age"] = person.age;

    return j_person;
}
Json::Value Client::createSimpleRequest(long number)
{
    Json::Value object;
    object["number"] = number;

    return object;
}

Json::Value Client::createSimpleRequest(int number)
{
    Json::Value object;
    object["delay"] = number;

    return object;
}
Json::Value Client::createSimpleRequest(std::string key)
{

    Json::Value object;
    object["key"] = key;

    return object;
}
Json::Value Client::createSimpleRequest(std::vector<std::string> keys)
{
    Json::Value object;
    object["name"] = keys[1];
    object["value"] = keys[2];

    return object;
}

int main(int argc, char const *argv[])
{
    Client client;
    if (!client.connectToServer())
        return 0;
    else
        std::cout << "client connected\n";

    bool close_connection = false;
    std::vector<std::string> commands = client.getCommands();

    client.printIntro();

    while (!close_connection)
    {
        std::string data;
        std::getline(std::cin, data);

        if (data.empty())
            client.printIntro();
        else
        {
            close_connection = client.selectCommand(data);
        }
    }
    return 0;
}
