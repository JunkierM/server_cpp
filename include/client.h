#pragma once
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <vector>
#include "json/json.h"
#include <chrono>
#include <algorithm>

#include "helpers.h"

#define PORT 8080
#define SERVER_ADRESS "127.0.0.1"

class Client : public Helpers
{

public:
    // Client();
    bool connectToServer();
    std::vector<std::string> getCommands();
    void printIntro();
    bool selectCommand(std::string input);
    Json::Value createObject(std::string first_name, std::string last_name, std::string age);
    Json::Value createSimpleRequest(long number);
    Json::Value createSimpleRequest(int number);
    Json::Value createSimpleRequest(std::string key);
    Json::Value createSimpleRequest(std::vector<std::string> keys);
    void sendReadOperation(int arg, std::vector<std::string> inputs, Json::Value &req, Command command);
    Json::Value createObject(Person person);

private:
    bool _hasOnlyDigits(const std::string s);

    void _getValuesForUpdate(Person &person);
    long _getUptime();
    void _readData();
    void _sendData(Json::Value data);
    // void _decodeMessage(const char *buffer);
    std::vector<std::string> _parseCommand(std::string input);
    std::chrono::steady_clock::time_point _start_time;

    std::string _all_commands;
    int _sock{0}, _valread;
    struct sockaddr_in _serv_addr;
    char _buffer[1024] = {0};
};