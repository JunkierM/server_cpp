#include "server.h"
bool Server::startServer()
{

    for (_i = 0; _i < _max_clients; _i++)
        _client_socket[_i] = 0;

    if ((_master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        std::cout << "socket failed\n";
        return false;
    }

    if (setsockopt(_master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&_opt,
                   sizeof(_opt)) < 0)
    {
        std::cout << "setsockopt failed\n";
        return false;
    }

    //type of socket created
    _address.sin_family = AF_INET;
    _address.sin_addr.s_addr = INADDR_ANY;
    _address.sin_port = htons(PORT);

    if (bind(_master_socket, (struct sockaddr *)&_address, sizeof(_address)) < 0)
    {
        std::cout << "bind failed\n";
        return false;
    }
    _start_time = std::chrono::steady_clock::now();
    return true;
}

ServerQuery Server::readClientMsg()
{
    ServerQuery db_query;
    if (listen(_master_socket, 3) < 0)
        std::cout << "listen failed\n";
    _addrlen = sizeof(_address);
    while (true)
    {
        FD_ZERO(&_readfds);
        FD_SET(_master_socket, &_readfds);
        _max_sd = _master_socket;
        for (_i = 0; _i < _max_clients; _i++)
        {

            _sd = _client_socket[_i];
            if (_sd > 0)
                FD_SET(_sd, &_readfds);
            if (_sd > _max_sd)
                _max_sd = _sd;
        }

        _activity = select(_max_sd + 1, &_readfds, NULL, NULL, NULL);

        if ((_activity < 0) && (errno != EINTR))
            std::cout << "select failed\n";
        if (FD_ISSET(_master_socket, &_readfds))
        {
            if ((_new_socket = accept(_master_socket,
                                      (struct sockaddr *)&_address, (socklen_t *)&_addrlen)) < 0)
            {
                std::cout << "accept failed\n";
                exit(-1);
            }

            for (_i = 0; _i < _max_clients; _i++)
            {
                if (_client_socket[_i] == 0)
                {
                    _client_socket[_i] = _new_socket;
                    break;
                }
            }
        }

        for (_i = 0; _i < _max_clients; _i++)
        {
            _sd = _client_socket[_i];
            if (FD_ISSET(_sd, &_readfds))
            {
                if ((_valread = read(_sd, _buffer, 1024)) == 0)
                {
                    close(_sd);
                    _client_socket[_i] = 0;
                }
                else
                {
                    Json::Value client_msg = _decodeMessage(_buffer);
                    db_query = _parseJsonToQuery(client_msg);
                    return db_query;
                }
            }
        }
    }
    return db_query;
}

long Server::_getUptime()
{

    auto current_time = std::chrono::steady_clock::now();
    auto uptime = std::chrono::duration_cast<std::chrono::seconds>(current_time - _start_time);
    long x = uptime.count();
    return x;
}

ServerQuery Server::_parseJsonToQuery(Json::Value client_msg)
{
    ServerQuery db_query;
    Command command;
    Json::Value getCommand = client_msg["cmd"];
    std::string cmd_value = getCommand.asString();
    for (int i = 0; i < _commands.size(); i++)
        if (cmd_value == _commands[i])
            command = static_cast<Command>(i);

    switch (command)
    {
    case STAT:
    {
        getCommand = client_msg["args"]["number"];
        cmd_value = getCommand.asString();
        db_query.query_type = cmd_value + " s";

        db_query.type = STAT;
        return db_query;
        break;
    }

    case INC:
    {
        getCommand = client_msg["args"]["number"];
        cmd_value = getCommand.asString();
        db_query.type = INC;
        db_query.query = "SELECT count(Age) FROM db_A.Person where age=" + cmd_value;
        db_query.query_type = "count(Age)";

        return db_query;
        break;
    }
    break;
    case GET:
    {
        getCommand = client_msg["args"]["number"];
        cmd_value = getCommand.asString();
        db_query.type = GET;
        db_query.query = "SELECT count(Age) FROM " + TABLE_NAME + " where age=" + cmd_value;
        db_query.query_type = "count(Age)";

        return db_query;
        break;
    }

    case SLEEP:
    {
        db_query.type = SLEEP;
        getCommand = client_msg["args"]["delay"];
        cmd_value = getCommand.asString();
        int wait = std::stoi(cmd_value);
        std::this_thread::sleep_for(std::chrono::seconds(wait));
        return db_query;
        break;
    }
    case END:
    {
        db_query.type = END;
        return db_query;
        break;
    }
    case WRITE:
    {
        db_query.type = UPDATE;

        std::string id, name, lastname, age;
        getCommand = client_msg["args"]["id"];
        id = getCommand.asString();

        getCommand = client_msg["args"]["FirstName"];
        db_query.person.name = getCommand.asString();
        getCommand = client_msg["args"]["LastName"];
        db_query.person.lastname = getCommand.asString();
        getCommand = client_msg["args"]["Age"];
        age = getCommand.asString();
        db_query.person.age = std::stoi(age);

        if (!id.empty())
        {
            std::string column_name, new_value;
            db_query.person.id = std::stoi(id);

            if (!db_query.person.name.empty())
            {
                column_name = "FirstName";
                new_value = db_query.person.name;
                db_query.query_type = new_value;
            }
            else if (!db_query.person.lastname.empty())
            {
                column_name = "LastName";
                new_value = db_query.person.lastname;
                db_query.query_type = new_value;
            }
            else if (!age.empty())
            {

                column_name = "Age";
                new_value = age;
            }

            db_query.query = "UPDATE " + TABLE_NAME + " SET " + column_name + " = ? WHERE ID = " + id;
        }
        else if (!db_query.person.name.empty())
        {

            db_query.type = WRITE;
            db_query.query = "INSERT INTO " + TABLE_NAME + " (FirstName, LastName, Age) VALUES (?,?,?)";
            db_query.query_type = "";
        }
        else
            db_query.type = NONE;

        return db_query;
        break;
    }
    break;
    case READ:
    {
        getCommand = client_msg["args"]["key"];
        cmd_value = getCommand.asString();
        db_query.type = READ;
        db_query.query = "SELECT * FROM " + TABLE_NAME + " where id =" + cmd_value;
        db_query.query_type = "";

        return db_query;
        break;
    }

    break;
    case DEL:
    {
        getCommand = client_msg["args"]["key"];
        cmd_value = getCommand.asString();
        db_query.type = DEL;
        db_query.query = "DELETE FROM " + TABLE_NAME + " WHERE (id = '" + cmd_value + "')";
        db_query.query_type = "count(id)";
        db_query.person.id = std::stoi(cmd_value);
        return db_query;
        break;
    }
    break;

    default:
    {
        std::cout << "Error";
        break;
    }
    }
}

Json::Value Server::createObject(Person person)
{

    Json::Value j_person;
    j_person["id"] = person.id;
    j_person["FirstName"] = person.name;
    j_person["LastName"] = person.lastname;
    j_person["Age"] = person.age;
    j_person["status"] = "ok";

    return j_person;
}
Json::Value Server::createJsonObject(std::string data)
{
    Json::Value object;
    if (data.empty())
        object["status"] = "ok";
    else if (data == "error")
        object["status"] = "error";
    else
    {
        object["status"] = "ok";
        object["hints"] = data;
    }
    return object;
}

Json::Value Server::creatStatJson(std::string data)
{
    long uptime = _getUptime();
    int connections = 0;
    for (size_t i = 0; i < _max_clients; i++)
        _client_socket[i] != 0 ? connections++ : connections;
    Json::Value object;
    object["status"] = "ok";
    object["server uptime"] = std::to_string(uptime) + " s";
    object["client uptime"] = data;
    object["connected clients"] = connections;

    return object;
}

void Server::sendResponse(Json::Value data)
{
    std::string temp_data = data.toStyledString();
    const char *c = temp_data.c_str();
    send(_sd, c, strlen(c), 0);
}

int main(int argc, char const *argv[])
{
    std::string db_a = "db_A";
    std::string db_b = "db_B";

    DataBase db;
    DataBase logs;
    Server server;
    db.createConnectionToDB(db_a);
    std::cout << "Database connection established.\n";
    if (!server.startServer())
        return 0;
    else
        std::cout << "Server is running... \n";

    logs.createConnectionToDB(db_b);

    while (true)
    {
        ServerQuery db_query;
        db_query = server.readClientMsg();
        std::string response = "error";
        if (db_query.type == GET || db_query.type == INC)
            response = db.getDataFromDB(db_query);
        else if (db_query.type == DEL)
            response = db.deleteDataFromDB(db_query);
        else if (db_query.type == WRITE)
            response = db.writeDataToDB(db_query);
        else if (db_query.type == READ)
        {
            Person person = db.readDataFromDB(db_query);
            if (!person.name.empty())
            {
                Json::Value temp;
                temp = server.createObject(person);
                server.sendResponse(temp);
                continue;
            }
        }
        else if (db_query.type == UPDATE)
        {

            ServerQuery temp;
            temp.query = "SELECT * FROM " + server.TABLE_NAME + " where id =" + std::to_string(db_query.person.id);
            Person person = db.readDataFromDB(temp);
            if (!person.name.empty())
            {
                logs.saveLogs(db_query, person);
                response = db.updateDataToDB(db_query);
            }
            else
                response = "error";
        }

        else if (db_query.type == STAT)
        {
            Json::Value temp;
            temp = server.creatStatJson(db_query.query_type);
            server.sendResponse(temp);
            continue;
        }
        else if (db_query.type == END || db_query.type == SLEEP)
            response = "";
        else if (db_query.type == NONE)
            response = "error";

        server.sendResponse(server.createJsonObject(response));
    }

    return 0;
}
