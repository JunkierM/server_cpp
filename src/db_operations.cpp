#include "db_operations.h"

void DataBase::createConnectionToDB(std::string db_name)
{
    try
    {
        driver = get_driver_instance();
        connection = driver->connect(IPADRESS, USERNAME, PASSWORD);
        connection->setSchema(db_name);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
}

std::string DataBase::deleteDataFromDB(ServerQuery in_query)
{
    if (_getObjCount(in_query) == 0)
    {
        std::string response = "error";
        return response;
    }

    try
    {

        preparedStatement = connection->prepareStatement(in_query.query);
        preparedStatement->executeUpdate();
        return "";
    }
    catch (const std::exception &e)
    {
        std::cout << "Get data from database failed! " << e.what() << '\n';
        std::string response = "error";
        return response;
    }
}
Person DataBase::readDataFromDB(ServerQuery in_query)
{

    Person person;
    try
    {
        statement = connection->createStatement();
        result = statement->executeQuery(in_query.query);
        while (result->next())
        {
            person.id = result->getInt("id");
            person.name = result->getString("FirstName");
            person.lastname = result->getString("LastName");
            person.age = result->getInt("Age");
        }
        return person;
    }
    catch (const std::exception &e)
    {
        std::cout << "Get data from database failed!" << e.what() << '\n';
        return person;
    }
}

std::string DataBase::writeDataToDB(ServerQuery in_query)
{

    preparedStatement = connection->prepareStatement(in_query.query);
    preparedStatement->setString(1, in_query.person.name);
    preparedStatement->setString(2, in_query.person.lastname);
    preparedStatement->setInt(3, in_query.person.age);

    preparedStatement->executeUpdate();

    return "";
}

std::string DataBase::updateDataToDB(ServerQuery in_query)
{

    preparedStatement = connection->prepareStatement(in_query.query);

    if (!in_query.query_type.empty())
        preparedStatement->setString(1, in_query.query_type);
    else
        preparedStatement->setInt(1, in_query.person.age);

    preparedStatement->executeUpdate();

    return "";
}

std::string DataBase::getDataFromDB(ServerQuery in_query)
{

    try
    {
        statement = connection->createStatement();
        result = statement->executeQuery(in_query.query);

        if (result->rowsCount() != 1)
        {
            throw std::invalid_argument("No row with ID in table area");
        }
        else
        {
            std::string response;

            while (result->next())
            {
                int temp;

                temp = result->getInt(in_query.query_type);
                response = std::to_string(temp);
            }
            return response;
        }
    }
    catch (const std::exception &e)
    {
        std::cout << "Get data from database failed! " << e.what() << '\n';
        std::string response = "error";
        return response;
    }
}

int DataBase::_getObjCount(ServerQuery in_query)
{

    try
    {
        statement = connection->createStatement();
        result = statement->executeQuery("SELECT " + in_query.query_type + " FROM " + TABLE_NAME + " WHERE id = " + std::to_string(in_query.person.id));

        if (result->rowsCount() != 1)
        {
            throw std::invalid_argument("No row with ID in table area");
        }
        else
        {
            while (result->next())
            {
                int response;
                response = result->getInt(in_query.query_type);
                return response;
            }
            return 0;
        }
    }
    catch (const std::exception &e)
    {
        std::cout << "Get data from database failed! " << e.what() << '\n';
        return 0;
    }
}

void DataBase::saveLogs(ServerQuery in_query, Person person)
{
    std::string column_name, new_value, old_value, query;
    old_value = "xx";

    if (!in_query.person.name.empty())
    {
        column_name = "FirstName";
        new_value = in_query.person.name;
        old_value = person.name;
    }
    else if (!in_query.person.lastname.empty())
    {
        column_name = "LastName";
        new_value = in_query.person.lastname;
        old_value = person.lastname;
    }
    else
    {
        column_name = "Age";
        new_value = std::to_string(in_query.person.age);
        old_value = std::to_string(person.age);
    }

    query = "INSERT INTO db_B.logs (changed_id, edited_column, old_value, new_value) VALUES (?,?,?,?)";

    preparedStatement = connection->prepareStatement(query);

    preparedStatement->setInt(1, in_query.person.id);
    preparedStatement->setString(2, column_name);
    preparedStatement->setString(3, old_value);
    preparedStatement->setString(4, new_value);

    preparedStatement->executeUpdate();
}